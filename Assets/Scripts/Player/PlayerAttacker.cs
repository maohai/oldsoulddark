using UnityEngine;

namespace SG {
    public class PlayerAttacker : MonoBehaviour {
        AnimatorHandler animatorHandler;
        public string lastAttack;
        InputHandler inputHandler;
        public WeaponSlotManager weaponSlotManager;
        private void Awake() {
            weaponSlotManager = GetComponent<WeaponSlotManager>();
            animatorHandler = GetComponent<AnimatorHandler>();
            inputHandler = GetComponent<InputHandler>();

        }
        public void HandleLightAttack(WeaponItem weapon){
            if (weapon.OH_Heavy_Attack_1 == "" || weapon.OH_Light_Attack_1 == "" ||
                weapon.OH_Light_Attack_2 == "") return;
            weaponSlotManager.attackingWeapon = weapon;
            animatorHandler.PlayerTargetAnimation(weapon.OH_Light_Attack_1, true);
            lastAttack = weapon.OH_Light_Attack_1;
        }
        public void HandleHeavyAttack(WeaponItem weapon) {
            if (weapon.OH_Heavy_Attack_1 == "" || weapon.OH_Light_Attack_1 == "" ||
                weapon.OH_Light_Attack_2 == "") return;
            weaponSlotManager.attackingWeapon = weapon;
            animatorHandler.PlayerTargetAnimation(weapon.OH_Heavy_Attack_1, true);
            lastAttack = weapon.OH_Heavy_Attack_1;
        }
        public void HandleWeaponCombo(WeaponItem weapon) {
            if (inputHandler.canComboFlag) {
                animatorHandler.anim.SetBool("CanDoCombo", false);
                if (lastAttack == weapon.OH_Light_Attack_1)
                    animatorHandler.PlayerTargetAnimation(weapon.OH_Light_Attack_2, true);
            }
        }
    }
}