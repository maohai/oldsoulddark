using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG {
    public class AnimatorHandler : MonoBehaviour {
        public Animator anim;
        public InputHandler inputHandler;
        public PlayerLocomotion playerLocomotion;
        public PlayerManager playerManager;
        int vertical;
        int horizontal;
        public bool canRotate;
        public void Initialize() {
            anim = GetComponent<Animator>();
            inputHandler = GetComponentInParent<InputHandler>();
            playerLocomotion = GetComponentInParent<PlayerLocomotion>();
            playerManager = GetComponentInParent<PlayerManager>();
            vertical = Animator.StringToHash("Vertical");
            horizontal = Animator.StringToHash("Horizontal");
        }

        public void UpdateAnimatorValues(float verticalMovement,float horizontalMovement, bool isSprinting) {
            #region Vertical
            float v = 0;
            if (verticalMovement is > 0 and < 0.55f) v = 0.5f;
            else if (verticalMovement > 0.55f) v = 1f;
            else if (verticalMovement is > -0.55f and < 0f) v = -0.5f;
            else if (verticalMovement < -0.55f) v = -1f;
            else v = 0f;
            #endregion
            #region Horizontal
            float h = 0;
            if (horizontalMovement is > 0 and < 0.55f) h = 0.5f;
            else if (horizontalMovement > 0.55f) h = 1f;
            else if (horizontalMovement is > -0.55f and < 0f) h = -0.5f;
            else if (horizontalMovement < -0.55f) h = -1f;
            else h = 0f;
            #endregion

            if (isSprinting) {
                v = 2;
                h = horizontalMovement;//冲刺锁定了水平方向,不让乱动
            }

            //这两句代码控制角色动画的前后左右的播放速度
            anim.SetFloat(vertical, v, 0.1f, Time.deltaTime);
            anim.SetFloat(horizontal, h, 0.1f, Time.deltaTime);
        }

        public void PlayerTargetAnimation(string targetAnim,bool isInteracting) {
            anim.applyRootMotion = isInteracting;//是否应用跟骨骼运动
            anim.SetBool("isInteracting", isInteracting);
            anim.CrossFade(targetAnim, 0.2f);//淡入到其他动画
        }

        public void CanRotate() => canRotate = true;
        public void StopRotate() => canRotate = false;
        public void EnableCombo() {
            anim.SetBool("CanDoCombo", true);
        }
        public void DisableCombo() {
            anim.SetBool("CanDoCombo", false);
        }
        private void OnAnimatorMove() {//动画移动时
            if (!playerManager.isInteracting) return;
            float delta = Time.deltaTime;
            playerLocomotion.rigidbody.drag = 0;//拖动设置为0
            Vector3 deltaPosition = anim.deltaPosition;//动画位置
            deltaPosition.y = 0;//锁定y轴
            Vector3 velocity = deltaPosition / delta;
            playerLocomotion.rigidbody.velocity = velocity;//运动
        }
    }
}
