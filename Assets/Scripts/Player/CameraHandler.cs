using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG {
    public class CameraHandler : MonoBehaviour{
        public InputHandler inputHandler;
        public Transform targetTransform;
        public Transform cameraTransform;
        public Transform cameraPivotTransform;
        private Transform myTransform;
        private Vector3 cameraTransformPosition;//摄像机位置
        public LayerMask ignoreLayers;//防止摄像机碰撞
        private Vector3 cameraFollowVelocity = Vector3.zero;//防止摄像机抖动

        public static CameraHandler singleton;//单例

        public float lookSpeed = 0.1f;
        public float followSpeed = 0.1f;
        public float pivotSpeed = 0.1f;

        private float targetPosition;//防止摄像机不会进入对象体内
        private float defaultPosition;
        private float lookAngle;
        private float pivotAngle;
        public float minimumPivot = -35;
        public float maximumPivot = 35;

        public float cameraSphereRadius = 0.2f;//碰撞体半径
        public float cameraCollisionOffset = 0.2f;//距离
        public float minimumCollisionOffset = 0.2f;//最低限度距离

        public Transform currentLockOnTarget;
        
        private List<CharacterManager> availableTargets = new List<CharacterManager>();
        public Transform nearestLockOnTarget;
        public Transform leftLockTarget;
        public Transform rightLockTarget;
        public float maximumLockOnDistance = 30;
        private void Awake() {
            singleton = this;
            myTransform = transform;
            defaultPosition = cameraTransform.localPosition.z;//默认位置
            ignoreLayers = ~(1 << 8 | 1 << 9 | 1 << 10);//忽略其它层
            targetTransform = FindObjectOfType<PlayerManager>().transform;
            inputHandler = FindObjectOfType<InputHandler>();
        }

        /// <summary>
        /// 摄像机跟随
        /// </summary>
        /// <param name="delta"></param>
        public void FollowTarget(float delta) {
            Vector3 targetPosition = Vector3.SmoothDamp(
                myTransform.position, 
                targetTransform.position, 
                ref cameraFollowVelocity, 
                delta);
            myTransform.position = targetPosition;//摄像机位置
            HandleCameraCollisions(delta);
        }
        /// <summary>
        /// 摄像机旋转
        /// </summary>
        /// <param name="delta"></param>
        /// <param name="mouseXInput"></param>
        /// <param name="mouseYInput"></param>
        public void HandleCameraRotation(float delta,float mouseXInput,float mouseYInput) {
            if (!inputHandler.lockOnFlag){
                lookAngle += (mouseXInput * lookSpeed) / delta;
                pivotAngle -= (mouseYInput * pivotSpeed) / delta;
                pivotAngle = Mathf.Clamp(pivotAngle, minimumPivot, maximumPivot);
                Debug.Log("mouseXInput:"+mouseXInput);
                Debug.Log("mouseYInput:"+mouseYInput);
                Vector3 rotation = Vector3.zero;
                rotation.y = lookAngle;//选装摄像机y轴
                Quaternion targetRotation = Quaternion.Euler(rotation);
                myTransform.rotation = targetRotation;//人物旋转
            
                rotation = Vector3.zero;
                rotation.x = pivotAngle;//旋转摄像机x轴
                targetRotation = Quaternion.Euler(rotation);
                cameraPivotTransform.localRotation = targetRotation;//摄像机旋转
            }
            else{
                float velocity = 0;
                Vector3 dir = currentLockOnTarget.position - transform.position;
                dir.Normalize();
                dir.y = 0;
                var targetRotation = Quaternion.LookRotation(dir);
                transform.rotation = targetRotation;
                dir = currentLockOnTarget.position - cameraPivotTransform.position;
                dir.Normalize();
                targetRotation = Quaternion.LookRotation(dir);
                var eulerAngle = targetRotation.eulerAngles;
                eulerAngle.y = 0;
                cameraPivotTransform.localEulerAngles = eulerAngle;
            }

        }
        /// <summary>
        /// 摄像机碰撞检测
        /// </summary>
        /// <param name="delta"></param>
        private void HandleCameraCollisions(float delta) {
            targetPosition = defaultPosition;
            RaycastHit hit;//射线检测
            //视角方向减去轴心点方向
            Vector3 direction = cameraTransform.position - cameraPivotTransform.position;
            //射线检测到这个球的时候,对摄像机和碰到的点进行判断,然后求出距离,进而移动摄像机的距离
            if(Physics.SphereCast(
                cameraPivotTransform.position,//起始点
                cameraSphereRadius,//半径
                direction,//方向
                out hit,//相交的物体信息
                Mathf.Abs(targetPosition),//长度
                ignoreLayers//忽略的层级
                )){
                float dis = Vector3.Distance(cameraPivotTransform.position, hit.point);
                targetPosition = -(dis - cameraCollisionOffset);//摄像机移动的距离
            }
            //如果小于最小距离,直接为负的
            if (Mathf.Abs(targetPosition) < minimumCollisionOffset)
                targetPosition = -minimumCollisionOffset;
            //targetPosition为z轴位置
            cameraTransformPosition.z = Mathf.Lerp(cameraTransform.localPosition.z, targetPosition, delta / 0.2f);
            cameraTransform.localPosition = cameraTransformPosition;//改变摄像机位置
        }

        public void HandleLockOn(){
            float shortestDistance = Mathf.Infinity;
            float shortestDistanceOfLeftTarget = Mathf.Infinity;
            float shortestDistanceOfRightTarget = Mathf.Infinity;
            var colliders = Physics.OverlapSphere(targetTransform.position, 26);
            foreach (var c in colliders){
                var character = c.GetComponent<CharacterManager>();
                if (character != null){
                    var lockTargetDirection = character.transform.position - targetTransform.position;
                    var distanceFromTarget = Vector3.Distance(targetTransform.position, character.transform.position);
                    var viewableAngle = Vector3.Angle(lockTargetDirection, cameraTransform.forward);

                    if (character.transform.root != targetTransform.transform.root 
                        && viewableAngle > -50 
                        && viewableAngle < 50 && distanceFromTarget <= maximumLockOnDistance){
                        availableTargets.Add(character);
                    }
                }
            }
            foreach (var target in availableTargets){
                var distanceFromTarget = Vector3.Distance(targetTransform.position, target.transform.position);
                if (distanceFromTarget < shortestDistance){
                    shortestDistance = distanceFromTarget;
                    nearestLockOnTarget = target.LockOnTransform;
                }

                if (inputHandler.lockOnFlag && currentLockOnTarget != null){
                    Vector3 relativeEnemyPosition =
                        currentLockOnTarget.InverseTransformPoint(target.transform.position);
                    var distanceFromLeftTarget = currentLockOnTarget.transform.position.x - target.transform.position.x;
                    var distanceFromRightTarget = currentLockOnTarget.transform.position.x + target.transform.position.x;
                    if (relativeEnemyPosition.x > 0.00 && distanceFromLeftTarget < shortestDistanceOfLeftTarget){
                        shortestDistanceOfLeftTarget = distanceFromLeftTarget;
                        leftLockTarget = target.LockOnTransform;
                    }
                    if (relativeEnemyPosition.x < 0.00 && distanceFromLeftTarget < shortestDistanceOfRightTarget){
                        shortestDistanceOfRightTarget = distanceFromRightTarget;
                        rightLockTarget = target.LockOnTransform;
                    }
                }
            }
        }
        
        public void ClearLockOnTargets(){
            availableTargets.Clear();
            nearestLockOnTarget = null;
            currentLockOnTarget = null;
        }
    }
}
