using UnityEngine;

namespace SG {
    public class PlayerStats : MonoBehaviour {
        public int healthLevel = 10;
        public int maxHealth;
        public int currentHealth;

        public HealthBar healthBar;

        public AnimatorHandler animatorHandler;

        public int staminaLevel = 10;
        public int maxStamina;
        public int currentStamina;
        public StaminaBar staminaBar;
        private void Awake() {
            staminaBar = FindObjectOfType<StaminaBar>();
            healthBar = FindObjectOfType<HealthBar>();
            animatorHandler = GetComponent<AnimatorHandler>();
        }
        private void Start() {
            maxHealth = SetMaxHealthFromHealthLecel();
            currentHealth = maxHealth;
            healthBar.SetMaxHealth(maxHealth);
            staminaBar.SetMaxStamina(maxHealth);
            currentStamina = SetMaxStaminaFromStaminaLevel();
            animatorHandler = GetComponent<AnimatorHandler>();
        }
        private int SetMaxHealthFromHealthLecel() {
            maxHealth = healthLevel * 10;
            return maxHealth;
        }
        private int SetMaxStaminaFromStaminaLevel() {
            maxStamina = staminaLevel * 10;
            return maxStamina;
        }
        public void TakeStaminaDamage(int damage) {
            currentStamina -= damage;
            staminaBar.SetCurrentHealth(currentStamina);
        }
        public void TakeDamage(int damage) {
            currentHealth = currentHealth - damage;
            healthBar.SetCurrentHealth(currentHealth);//更新血条

            animatorHandler.PlayerTargetAnimation("Damage_01", true);
            if (currentHealth <= 0) {
                currentHealth = 0;
                animatorHandler.PlayerTargetAnimation("Dead_01", true);
            }
        }
    }
}