using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG {
    public class InputHandler : MonoBehaviour {
        public float horizontal;
        public float vertical;
        public float moveAmount;
        public float mouseX;
        public float mouseY;

        public bool b_Input;
        public bool rb_input;
        public bool rt_input;
        public bool inventory_input;
        public bool lockOnInput;
        public bool rollFlag;
        public bool inventoryFlag;
        public bool sprintFlag;
        public bool canComboFlag;
        public bool lockOnFlag;
        public float roolInputTimer;
        public bool right_Stick_Right_Input;
        public bool right_Stick_Left_Input;


        public bool d_Pad_Up;
        public bool d_Pad_Down;
        public bool d_Pad_Left;
        public bool d_Pad_Right;
        public bool f_Input;
        public bool jump_input;

        PlayerController inputActions;
        PlayerAttacker playerAttacker;
        PlayerInventory playerInventory;
        PlayerManager playerManager;
        CameraHandler cameraHandler;

        Vector2 movementInput;
        Vector2 cameraInput;
        
        UIManager uiManager;


        private void Awake() {
            playerAttacker = GetComponent<PlayerAttacker>();
            playerInventory = GetComponent<PlayerInventory>();
            playerManager = GetComponent<PlayerManager>();
            cameraHandler = FindObjectOfType<CameraHandler>();
            uiManager = FindObjectOfType<UIManager>();
        }
        public void OnEnable(){
            Glob.Ins.OpenMouse = false;
            if(inputActions == null) {
                inputActions = new PlayerController();
                inputActions.PlayerMovenment.Movement.performed +=
                    inputActions => movementInput = inputActions.ReadValue<Vector2>();//添加动作表
                inputActions.PlayerMovenment.Camera.performed += i => cameraInput = i.ReadValue<Vector2>();
                inputActions.PlayerActions.RB.performed += i => rb_input = true;
                inputActions.PlayerActions.RT.performed += i => rt_input = true;
                inputActions.PlayerActions.F.performed += i => f_Input = true;
                inputActions.PlayerActions.Jump.performed += i => jump_input = true;
                inputActions.PlayerQuickSlot.Right.performed += i => d_Pad_Right = true;
                inputActions.PlayerQuickSlot.Left.performed += i => d_Pad_Left = true;
                inputActions.PlayerActions.Inventory.performed += i => inventory_input = true;
                inputActions.PlayerActions.LockOn.performed += i => lockOnInput = true;
                inputActions.PlayerMovenment.LockOnTargetLeft.performed += i => right_Stick_Left_Input = true;
                inputActions.PlayerMovenment.LockOnTargetRight.performed += i => right_Stick_Right_Input = true;
            }
            inputActions.Enable();//启用表
        }
        public void OnDisable() {
            inputActions.Disable();//禁用表
        }
        public void TickInput(float delta) {
            MoveInput(delta);
            HandleRollInput(delta);
            HandleAttackInput(delta);
            HandleQuickSlotsInput();
            HandleInventoryInput();
            HandleLockOnInput();
        }
        public void MoveInput(float delta) {
            horizontal = movementInput.x;
            vertical = movementInput.y;
            moveAmount = Mathf.Clamp01(Mathf.Abs(horizontal) + Mathf.Abs(vertical));
            mouseX = cameraInput.x;
            mouseY = cameraInput.y;
        }
        private void HandleRollInput(float delta) {
            b_Input = inputActions.PlayerActions.Roll.phase == UnityEngine.InputSystem.InputActionPhase.Performed;
            sprintFlag = b_Input;
            if (b_Input) {
                roolInputTimer += delta;
            }
            else {
                if(roolInputTimer is > 0 and < 0.5f) {//如果按键小于0.5则是滚动
                    sprintFlag = false;
                    rollFlag = true;
                }
                roolInputTimer = 0;
            }
        }
        private void HandleAttackInput(float delta) {

            if (rb_input)
                if (playerManager.canDoCombo) {
                    canComboFlag = true;
                    playerAttacker.HandleWeaponCombo(playerInventory.rightWeapon);
                    canComboFlag = false;
                }
                else {
                    if (playerManager.isInteracting || playerManager.canDoCombo) return;
                    playerAttacker.HandleLightAttack(playerInventory.rightWeapon);
                }

            if (rt_input)
                playerAttacker.HandleHeavyAttack(playerInventory.rightWeapon);

        }
        private void HandleQuickSlotsInput() {
            if (d_Pad_Right) playerInventory.ChangeRightWeapon();
            else if (d_Pad_Left) playerInventory.ChangeLeftWeapon();
        }
        private void HandleInventoryInput() {
            if (inventory_input) {
                inventoryFlag = !inventoryFlag;
                if (inventoryFlag) {
                    uiManager.OpenSelectWindow();
                    uiManager.UpdateUI();
                    uiManager.hubWindows.SetActive(false);
                }
                else {
                    uiManager.CloseSelectWindow();
                    uiManager.hubWindows.SetActive(true);
                    uiManager.weaponInventoryWindows.SetActive(false);
                }
            }
        }

        private void HandleLockOnInput(){
            if (lockOnInput && !lockOnFlag){
                lockOnInput = false;
                cameraHandler.HandleLockOn();
                if (cameraHandler.nearestLockOnTarget != null){
                    cameraHandler.currentLockOnTarget = cameraHandler.nearestLockOnTarget;
                    lockOnFlag = true;
                }
            }else if (lockOnInput && lockOnFlag){
                lockOnInput = false;
                lockOnFlag = false;
                cameraHandler.ClearLockOnTargets();
            }

            if (lockOnFlag && right_Stick_Left_Input){
                right_Stick_Left_Input = false;
                cameraHandler.HandleLockOn();
                if (cameraHandler.leftLockTarget != null)
                    cameraHandler.currentLockOnTarget = cameraHandler.leftLockTarget;
            }
            if (lockOnFlag && right_Stick_Right_Input){
                right_Stick_Right_Input = false;
                cameraHandler.HandleLockOn();
                if (cameraHandler.rightLockTarget != null)
                    cameraHandler.currentLockOnTarget = cameraHandler.rightLockTarget;
            }
        }


    }
}