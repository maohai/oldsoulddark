using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG {
    public class PlayerLocomotion : MonoBehaviour {

        PlayerManager playerManager;
        Transform cameraObject;
        InputHandler inputHandler;
        public Vector3 moveDirection;
        [HideInInspector] public Transform myTransform;
        [HideInInspector] public AnimatorHandler animatorHandler;
        public new Rigidbody rigidbody;
        public GameObject mormalCamera;

        [Header("Ground & Air Detection Stats")]
        [SerializeField] float groundDetectionRayStartPoint = 0.5f;//射线检测距离
        [SerializeField] float minimumDistanceNeededToBeginFall = 1f;//最小下落距离
        [SerializeField] float groundDirectionRayDistance = 0.2f;//检测半径
        LayerMask ignoreForGroundCheck;//忽略的层级
        public float inAirTimer;//空中时间

        [Header("MoveMent Stats")]
        [SerializeField] float walkingSpeed = 1f;//行走速度
        [SerializeField] float movementSpeed = 5f;//速度
        [SerializeField] float sprintSpeed = 7f;//冲刺速度
        [SerializeField] float rotationSpeed = 10f;//转身速度
        [SerializeField] float fallingSpeed = 45f;//下落速度

        private void Start() {
            playerManager = GetComponent<PlayerManager>();
            rigidbody = GetComponent<Rigidbody>();
            inputHandler = GetComponent<InputHandler>();
            animatorHandler = GetComponentInChildren<AnimatorHandler>();
            cameraObject = Camera.main.transform;
            myTransform = transform;
            animatorHandler.Initialize();

            playerManager.isGrounede = true;
            ignoreForGroundCheck = ~(1 << 8 | 1 << 11);
        }

        #region Movement
        Vector3 normalVector;
        Vector3 targetPostion;
        private void HandleRoatation(float delta) {
            Vector3 targetDir = Vector3.zero;
            float moceOverrider = inputHandler.moveAmount;
            targetDir = cameraObject.forward * inputHandler.vertical;
            targetDir += cameraObject.right * inputHandler.horizontal;
            targetDir.Normalize();
            targetDir.y = 0;
            if (targetDir == Vector3.zero)
                targetDir = myTransform.forward;
            float rs = rotationSpeed;
            Quaternion tr = Quaternion.LookRotation(targetDir);
            Quaternion targetRotation = Quaternion.Slerp(myTransform.rotation, tr, rs * delta);
            myTransform.rotation = targetRotation;
        }

        public void HandleMovement(float delta) {
            if (inputHandler.rollFlag || playerManager.isInteracting) return;//处于翻滚状态,直接返回
            moveDirection = cameraObject.forward * inputHandler.vertical;
            moveDirection += cameraObject.right * inputHandler.horizontal;
            moveDirection.Normalize();
            moveDirection.y = 0;//冻结y轴
            float speed = movementSpeed;
            if (inputHandler.sprintFlag && inputHandler.moveAmount > 0.5f) {
                speed = sprintSpeed;
                playerManager.isSprinting = true;//冲刺设置为true
                moveDirection *= speed;
            }
            else {
                if(inputHandler.moveAmount < 0.5f) {
                    moveDirection *= walkingSpeed;
                    playerManager.isSprinting = false;
                }else {
                    moveDirection *= speed;
                    playerManager.isSprinting = false;
                }
            }
            Vector3 projectedVelocity = Vector3.ProjectOnPlane(moveDirection, normalVector);
            rigidbody.velocity = projectedVelocity;
            animatorHandler.UpdateAnimatorValues(inputHandler.moveAmount, 0, playerManager.isSprinting);
            if (animatorHandler.canRotate) HandleRoatation(delta);
        }

        public void HandleRollingAndSprinting(float delta) {

            if (animatorHandler.anim.GetBool("isInteracting")) return;
            if (inputHandler.rollFlag) {
                moveDirection = cameraObject.forward * inputHandler.vertical;
                moveDirection += cameraObject.right * inputHandler.horizontal;
                if (inputHandler.moveAmount > 0) {
                    animatorHandler.PlayerTargetAnimation("Rolling", true);
                    moveDirection.y = 0;//锁定y轴
                    Quaternion rollRotation = Quaternion.LookRotation(moveDirection);
                    myTransform.rotation = rollRotation;
                }
                else animatorHandler.PlayerTargetAnimation("Backstep", true);
            }
        }

        public void HandleFalling(float delta,Vector3 moveDirection) {
            playerManager.isGrounede = false;
            RaycastHit hit;
            Vector3 origin = myTransform.position;
            origin.y += groundDetectionRayStartPoint;
            if (Physics.Raycast(origin, myTransform.forward, out hit, 0.4f))
                moveDirection = Vector3.zero;
            if (playerManager.isInAir) {
                rigidbody.AddForce(-Vector3.up * fallingSpeed);//给一个向下的力
                rigidbody.AddForce(moveDirection * fallingSpeed / 5);//给一个前的力,防止卡在边缘
            }
            Vector3 dir = moveDirection;
            dir.Normalize();
            origin = origin + dir * groundDirectionRayDistance;//起点+方向上的射线距离
            targetPostion = myTransform.position;
            Debug.DrawRay(origin, -Vector3.up * minimumDistanceNeededToBeginFall, Color.red, 0.1f, false);
            if(Physics.Raycast(origin,-Vector3.up,out hit, minimumDistanceNeededToBeginFall, ignoreForGroundCheck)) {
                normalVector = hit.normal;
                Vector3 tp = hit.point;
                playerManager.isGrounede = true;
                targetPostion.y = tp.y;
                if (playerManager.isInAir) {
                    if (inAirTimer > 0.5f) animatorHandler.PlayerTargetAnimation("Land", true);
                    else{
                        animatorHandler.PlayerTargetAnimation("Locomotion", false);
                        animatorHandler.PlayerTargetAnimation("Empty", false);
                    }
                    inAirTimer = 0;
                    playerManager.isInAir = false;
                }
            }
            else {
                if (playerManager.isGrounede)
                    playerManager.isGrounede = false;
                if (!playerManager.isInAir) {
                    if (!playerManager.isInteracting)
                        animatorHandler.PlayerTargetAnimation("Falling", true);
                    Vector3 vel = rigidbody.velocity;
                    vel.Normalize();
                    rigidbody.velocity = vel * (movementSpeed / 2);//空中移动速度
                    playerManager.isInAir = true;
                }
            }
            if (playerManager.isGrounede)
                if (playerManager.isInteracting || inputHandler.moveAmount > 0)
                    myTransform.position = Vector3.Lerp(myTransform.position, targetPostion, Time.deltaTime/0.1f);//除以0.1为了保证足够小才不会让玩家陷下去
                else myTransform.position = targetPostion;
        }
        #endregion
        
        public void HandleJumping() {
            if (playerManager.isInteracting) return;
            if(inputHandler.jump_input && inputHandler.moveAmount > 0) {
                moveDirection = cameraObject.forward * inputHandler.vertical;
                moveDirection = cameraObject.right * inputHandler.horizontal;
                animatorHandler.PlayerTargetAnimation("Jump", false);
                moveDirection.y = 0;
                Quaternion jumpRotation = Quaternion.LookRotation(moveDirection);
                myTransform.rotation = jumpRotation;
            }
        }

    }
}
