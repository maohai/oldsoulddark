using UnityEngine;

namespace SG {
    public class WeaponSlotManager : MonoBehaviour {
        WeaponHolderSlot leftHandSlot;
        WeaponHolderSlot rightHandSlot;

        DamageCollider leftHandDamageCollider;
        DamageCollider rightHandDamageCollider;
        
        Animator animator;
        QuiteUISlot quiteUISlot;
        
        public WeaponItem attackingWeapon;
        PlayerStats playerStates;
        private void Awake() {
            animator = GetComponent<Animator>();
            quiteUISlot = FindObjectOfType<QuiteUISlot>();//场景里查找这个组件
            
            playerStates = GetComponent<PlayerStats>();
            WeaponHolderSlot[] weaponHolderSlots = GetComponentsInChildren<WeaponHolderSlot>();
            foreach (var item in weaponHolderSlots)
                if (item.isLeftHandSlot) leftHandSlot = item;
                else if (item.isRightHandSlot) rightHandSlot = item;
        }
        public void LoadWeaponOnSlot(WeaponItem weaponItem, bool isLeft) {
            if (isLeft) {
                leftHandSlot.LoadWeaponModel(weaponItem); 
                LoadLeftWeaponDamageCollider();
                quiteUISlot.UpdateWeaponquickSlotsUI(true, weaponItem);
                if (weaponItem != null) animator.CrossFade(weaponItem.left_hand_idle, 0.2f);
                else animator.CrossFade("Left Arm Empty", 0.2f);
            }
            else {
                rightHandSlot.LoadWeaponModel(weaponItem); 
                LoadRightWeaponDamageCollider();
                quiteUISlot.UpdateWeaponquickSlotsUI(false, weaponItem);
                if (weaponItem != null) animator.CrossFade(weaponItem.right_hand_idle, 0.2f);
                else animator.CrossFade("Right Arm Empty", 0.2f);
            }
        }
        private void LoadLeftWeaponDamageCollider() {
            leftHandDamageCollider = leftHandSlot.currentWeaponModel.GetComponentInChildren<DamageCollider>();
        }
        private void LoadRightWeaponDamageCollider() {
            rightHandDamageCollider = rightHandSlot.currentWeaponModel.GetComponentInChildren<DamageCollider>();
        }

        public void OpenRightDamageCollider() {
            rightHandDamageCollider.EnableDamageCollider();
        }
        public void OpenLeftDamageCollider() {
            leftHandDamageCollider.EnableDamageCollider();
        }
        public void CloseRightDamageCollider() {
            rightHandDamageCollider.DisableDamageCollider();
        }
        public void CloseLeftDamageCollider() {
            leftHandDamageCollider.DisableDamageCollider();
        }
        public void DrainStaminaLightAttack() => playerStates.TakeStaminaDamage(Mathf.RoundToInt(attackingWeapon.baseStamina * attackingWeapon.lightAttackMultiplier));
        public void DrainStaminaHeavyAttack() => playerStates.TakeStaminaDamage(Mathf.RoundToInt(attackingWeapon.baseStamina * attackingWeapon.heavyAttackMultiplier));
        
    }
}