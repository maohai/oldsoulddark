using UnityEngine;
namespace SG {
    public class PlayerManager : CharacterManager {
        InputHandler inputHandler;
        Animator anim;
        CameraHandler cameraHandler;
        PlayerLocomotion playerLocomotion;

        public bool isInteracting;//是否按下Shift
        public bool isSprinting;//在冲刺
        public bool isInAir;//在空中
        public bool isGrounede;//在地面
        public bool canDoCombo;//能够连击
        
        InteractableUI interactableUI;
        public GameObject interactableGameObject;
        public GameObject itemInteractableGameObject;
        private void Awake() {
            inputHandler = GetComponent<InputHandler>();
            anim = GetComponentInChildren<Animator>();
            playerLocomotion = GetComponent<PlayerLocomotion>();
            cameraHandler = FindObjectOfType<CameraHandler>();
            interactableUI = FindObjectOfType<InteractableUI>();

        }
        private void Start() {
            inputHandler = GetComponent<InputHandler>();
            anim = GetComponentInChildren<Animator>();
            playerLocomotion = GetComponent<PlayerLocomotion>();
        }
        private void Update() {
            float delta = Time.deltaTime;

            isInteracting = anim.GetBool("isInteracting");
            canDoCombo = anim.GetBool("CanDoCombo");
            anim.SetBool("isInAir",isInAir);
            inputHandler.TickInput(delta);
            playerLocomotion.HandleRollingAndSprinting(delta);
            playerLocomotion.HandleJumping();
            CheckForInteractableObject();
        }
        private void FixedUpdate() {
            float delta = Time.fixedDeltaTime;
            playerLocomotion.HandleMovement(delta);
            playerLocomotion.HandleFalling(delta, playerLocomotion.moveDirection);

        }
        private void LateUpdate() {
            inputHandler.rollFlag = false;//翻滚一直设置为false,当按下的时候修改为true
            inputHandler.rb_input = false;
            inputHandler.rt_input = false;
            inputHandler.d_Pad_Up = false;
            inputHandler.d_Pad_Down = false;
            inputHandler.d_Pad_Left = false;
            inputHandler.d_Pad_Right = false;
            inputHandler.f_Input = false;
            inputHandler.jump_input = false;//  空格
            inputHandler.inventory_input = false;

            // isSprinting = inputHandler.b_Input;
            float delta = Time.fixedDeltaTime;
            if (cameraHandler != null) {
                cameraHandler.FollowTarget(delta);
                cameraHandler.HandleCameraRotation(delta, inputHandler.mouseX, inputHandler.mouseY);
            }
            if (isInAir)
                playerLocomotion.inAirTimer = playerLocomotion.inAirTimer + Time.deltaTime;
        }
        public void CheckForInteractableObject() {
            RaycastHit hit;
            if(Physics.SphereCast(transform.position,0.3f,transform.forward,out hit, 1f, cameraHandler.ignoreLayers)) {
                if(hit.collider.tag == "Interactable") {
                    Interactable interactableObject = hit.collider.GetComponent<Interactable>();
                    if (interactableObject != null) {
                        interactableUI.TxtInteractable.text = interactableObject.interactbleText;
                        interactableGameObject.SetActive(true);

                        if (inputHandler.f_Input) {
                            hit.collider.GetComponent<Interactable>().Interal(this);
                        }
                    }
                }
            }
            else {
                if (interactableGameObject != null)
                    interactableGameObject.SetActive(false);
                if (itemInteractableGameObject != null && inputHandler.f_Input) {
                    itemInteractableGameObject.SetActive(false);
                }
            }
        }


    }
}