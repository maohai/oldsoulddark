using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Glob : SingletonMono<Glob>{

    private bool openMouse = false;

    public bool OpenMouse{
        get => openMouse;
        set{
            openMouse = value;
            Refresh();
        }
    }


    public void Refresh(){
        //隐藏鼠标并锁定
        Cursor.visible = openMouse;//鼠标显示与隐藏
        if(openMouse)
            Cursor.lockState = CursorLockMode.None;//鼠标为锁定
        else Cursor.lockState = CursorLockMode.Locked;//鼠标为锁定
    }
}
