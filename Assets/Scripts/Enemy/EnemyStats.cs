using UnityEngine;

namespace SG {
    public class EnemyStats : MonoBehaviour {
        public int healthLevel = 10;
        public int maxHealth;
        public int currentHealth;

        Animator animator;

        private void Awake() {
            animator = GetComponent<Animator>();
        }
        private void Start() {
            maxHealth = SetMaxHealthFromHealthLecel();
            currentHealth = maxHealth;
        }
        private int SetMaxHealthFromHealthLecel() {
            maxHealth = healthLevel * 10;
            return maxHealth;
        }

        public void TakeDamage(int damage) {
            currentHealth = currentHealth - damage;

            animator.Play("Damage_01");
            if (currentHealth <= 0) {
                currentHealth = 0;
                animator.Play("Dead_01");
                //todo 死亡
            }
        }
    }
}