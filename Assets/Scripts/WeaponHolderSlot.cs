using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG {
    public class WeaponHolderSlot : MonoBehaviour {
        public Transform parentOverride;
        public bool isLeftHandSlot;
        public bool isRightHandSlot;
        public GameObject currentWeaponModel;

        public void LoadWeaponModel(WeaponItem weaponItem) {
            UnloadWeaponAndDestory();//先摧毁武器
            if (!weaponItem) {//为空就卸载当前武器并返回
                UnloadWeapon();
                return;
            }
            GameObject model = Instantiate(weaponItem.modelPrefab) as GameObject;
            if (model) {//初始化
                if (parentOverride) model.transform.parent = parentOverride;
                else model.transform.parent = transform;
                model.transform.localPosition = Vector3.zero;
                model.transform.localRotation = Quaternion.identity;
                model.transform.localScale = Vector3.one;
            }
            currentWeaponModel = model;
        }
        /// <summary>
        /// 卸载武器
        /// </summary>
        public void UnloadWeapon() {
            if (currentWeaponModel) 
                currentWeaponModel.SetActive(false);
        }

        /// <summary>
        /// 卸载并摧毁
        /// </summary>
        public void UnloadWeaponAndDestory() {
            if (currentWeaponModel)
                Destroy(currentWeaponModel);
        }
    }
}