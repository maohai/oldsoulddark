using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG {
    public class Interactable : MonoBehaviour {
        public float radius = 0.6f;
        public string interactbleText;

        private void OnDrawGizmosSelected() {
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(transform.position, radius);
        }

        public virtual void Interal(PlayerManager playerManager) {
            Debug.Log($"交互了");

        }
    }

}