using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SG {
    public class InteractableUI : MonoBehaviour {
        public Text TxtInteractable;
        public Text TxtItemInteractable;
        public RawImage ImgWeapon;
    }
}