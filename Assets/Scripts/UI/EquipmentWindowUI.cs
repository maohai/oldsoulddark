using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG{
    public class EquipmentWindowUI : MonoBehaviour{
        public bool rightHandSlot01Selected;
        public bool rightHandSlot02Selected;
        public bool leftHandSlot01Selected;
        public bool leftHandSlot02Selected;
        HandEquipmentSlotUI[] handEquipmentSlotUI;
        public void SelectRightHandSlot01() => rightHandSlot01Selected = true;
        public void SelectRightHandSlot02() => rightHandSlot02Selected = true;
        public void SelectLeftHandSlot01() => leftHandSlot01Selected = true;
        public void SelectLeftHandSlot02() => leftHandSlot02Selected = true;

        private void Awake(){
            handEquipmentSlotUI = GetComponentsInChildren<HandEquipmentSlotUI>();
        }

        public void LoadWeapinsOnEquipmentScreen(PlayerInventory playerInventory){
            foreach (var item in handEquipmentSlotUI){
                if(item.rightHnadSlot01) item.AddItem(playerInventory.weaponInRightHandSlots[0]);
                if(item.rightHnadSlot02) item.AddItem(playerInventory.weaponInRightHandSlots[1]);
                if(item.leftHnadSlot01) item.AddItem(playerInventory.weaponInLeftHandSlots[0]);
                if(item.leftHnadSlot02) item.AddItem(playerInventory.weaponInLeftHandSlots[1]);
            }
        }
    }

}