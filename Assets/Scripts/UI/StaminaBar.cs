using UnityEngine;
using UnityEngine.UI;

namespace SG {
    public class StaminaBar : MonoBehaviour {
        public Slider slider;
        public void SetMaxStamina(int MaxStamina) {
            slider.maxValue = MaxStamina;
            slider.value = MaxStamina;
        }
        public void SetCurrentHealth(int currentStamina) {
            slider.value = currentStamina;
        }
    }
}