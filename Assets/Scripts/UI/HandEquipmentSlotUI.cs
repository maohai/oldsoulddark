using UnityEngine;
using UnityEngine.UI;

namespace SG{
    public class HandEquipmentSlotUI : MonoBehaviour{
        public Image icon;
        private WeaponItem weapon;

        public bool rightHnadSlot01;
        public bool rightHnadSlot02;
        public bool leftHnadSlot01;
        public bool leftHnadSlot02;

        public void AddItem(WeaponItem newWeapon){
            weapon = newWeapon;
            icon.sprite = weapon.itemIcon;
            icon.enabled = true;
            gameObject.SetActive(true);
        }
        public void ClearItem(){
            weapon = null;
            icon.sprite = null;
            icon.enabled = false;
            gameObject.SetActive(false);
        }
    }
}
