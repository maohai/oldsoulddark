using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG {
    public class UIManager : MonoBehaviour {
        public PlayerInventory playerInventory;
        private EquipmentWindowUI equipmentWindowUI;
        [Header("UI Windos")]
        public GameObject hubWindows;
        public GameObject selectWindow;
        public GameObject weaponInventoryWindows;

        [Header("Weapon Inventory")]
        public GameObject weaponInventorySlotPrefab;
        public Transform weaponInventorySlotsParent;
        WeaponInventorySlot[] weaponInventorySlots;

        private void Awake(){
            equipmentWindowUI = FindObjectOfType<EquipmentWindowUI>();
        }

        private void Start() {
            weaponInventorySlots = weaponInventorySlotsParent.GetComponentsInChildren<WeaponInventorySlot>();
            if(equipmentWindowUI != null)
                equipmentWindowUI.LoadWeapinsOnEquipmentScreen(playerInventory);
        }
        public void UpdateUI() {
            for(int i = 0;i < weaponInventorySlots.Length; i++) {
                if(i < playerInventory.weaponInventory.Count) {
                    if(weaponInventorySlots.Length < playerInventory.weaponInventory.Count) {
                        Instantiate(weaponInventorySlotPrefab, weaponInventorySlotsParent); 
                        weaponInventorySlots = weaponInventorySlotsParent.GetComponentsInChildren<WeaponInventorySlot>();
                    }
                    weaponInventorySlots[i].AddImte(playerInventory.weaponInventory[i]);
                }
                else {
                    weaponInventorySlots[i].ClearInventorySlot();
                }
            }
        }
        public void OpenSelectWindow() {
            selectWindow.SetActive(true);
        }
        public void CloseSelectWindow() {
            selectWindow.SetActive(false);
        }
    }
}